/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_board.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/11 14:17:34 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/11 17:39:34 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_check_rows(t_fillit *bias, int num, int y)
{
	if (!bias->board[bias->coord[num].y[0] + y] ||
		!bias->board[bias->coord[num].y[1] + y] ||
		!bias->board[bias->coord[num].y[2] + y] ||
		!bias->board[bias->coord[num].y[3] + y])
		return (0);
	return (1);
}

int		ft_check_cols(t_fillit *bias, int num, int x, int y)
{
	if (ft_check_rows(bias, num, y))
	{
		if ((bias->board[bias->coord[num].y[0] + y][bias->coord[num].x[0] + x])
		&& (bias->board[bias->coord[num].y[1] + y][bias->coord[num].x[1] + x])
		&& (bias->board[bias->coord[num].y[2] + y][bias->coord[num].x[2] + x])
		&& (bias->board[bias->coord[num].y[3] + y][bias->coord[num].x[3] + x]))
			return (1);
	}
	return (0);
}

int		ft_check_board(t_fillit *bias, int num, int x, int y)
{
	if (!ft_check_cols(bias, num, x, y) ||
		!ft_check_rows(bias, num, y))
		return (0);
	return (1);
}

int		ft_find_dots(t_fillit *bias, int num, int x, int y)
{
	if (ft_check_board(bias, num, x, y))
		if ((bias->board[bias->coord[num].y[0] + y]
		[bias->coord[num].x[0] + x] == '.')
		&& (bias->board[bias->coord[num].y[1] + y]
		[bias->coord[num].x[1] + x] == '.')
		&& (bias->board[bias->coord[num].y[2] + y]
		[bias->coord[num].x[2] + x] == '.')
		&& (bias->board[bias->coord[num].y[3] + y]
		[bias->coord[num].x[3] + x] == '.'))
			return (1);
	return (0);
}
