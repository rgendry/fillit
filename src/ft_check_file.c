/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_file.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ramory-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 17:41:18 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/14 16:35:26 by ramory-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	find_shapes(char *buff, int *c, int i)
{
	if (buff[i] == '#')
	{
		if (buff[i - 1] && buff[i - 1] == '#')
			(*c)++;
		if (buff[i - 5] && buff[i - 5] == '#')
			(*c)++;
		if (buff[i + 1] && buff[i + 1] == '#')
			(*c)++;
		if (buff[i + 5] && buff[i + 5] == '#')
			(*c)++;
	}
}

int		ft_true_fig(char *buff)
{
	int i;
	int dot;
	int c;

	i = 0;
	dot = 0;
	c = 0;
	while (buff[i] == '#' || buff[i] == '.' || buff[i] == '\n')
	{
		find_shapes(buff, &c, i);
		if (buff[i] == '.')
			dot++;
		if ((i == 4 || i == 9 || i == 14 || i == 19) && buff[i] != '\n')
			return (0);
		i++;
	}
	if ((c == 6 || c == 8) && dot == 12)
		return (1);
	return (0);
}

int		ft_validation(char *buff, t_figure *coord)
{
	int arr[4];

	ft_bzero(arr, sizeof(arr));
	if (ft_true_fig(buff) == 1)
	{
		while (arr[0] < 21 && arr[1] < 4)
		{
			if (arr[0] == 4 || arr[0] == 9 || arr[0] == 14 || arr[0] == 19)
			{
				arr[2] = -1;
				arr[3]++;
			}
			if (buff[arr[0]] == '#')
			{
				coord->x[arr[1]] = arr[2];
				coord->y[arr[1]] = arr[3];
				arr[1]++;
			}
			arr[2]++;
			arr[0]++;
		}
		coord = ft_move_figure(coord);
		return (1);
	}
	return (0);
}
