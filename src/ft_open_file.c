/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_open_file.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ramory-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 18:37:43 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/14 16:47:56 by ramory-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_print_error(char *program)
{
	ft_putstr_fd("usage: ", STDERR_FILENO);
	ft_putstr_fd(program, STDERR_FILENO);
	ft_putstr_fd(" [input_file]\n", STDERR_FILENO);
}

t_figure	*ft_error(t_figure *coord)
{
	write(1, "error\n", 6);
	free(coord);
	return (NULL);
}

t_figure	*ft_open_file(char *argv, int *i)
{
	int			n;
	int			fd;
	char		buff[21];
	t_figure	*coord;

	if (!(coord = (t_figure *)malloc(sizeof(t_figure) * 26)))
		return (NULL);
	if ((fd = open(argv, O_RDONLY)) == -1)
		return (ft_error(coord));
	while ((n = read(fd, buff, 21)) > 0)
	{
		buff[n] = '\0';
		if (!ft_validation(buff, &coord[*i]))
			return (ft_error(coord));
		(*i)++;
	}
	if ((n == 0 && *i == 0) || ft_strstr(buff, "\n\n"))
		return (ft_error(coord));
	if (close(fd) == -1)
		return (ft_error(coord));
	return (coord);
}
