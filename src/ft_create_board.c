/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_board.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 18:05:05 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/11 17:39:37 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_erase_board(char **board)
{
	int i;

	i = 0;
	while (board[i])
	{
		free(board[i]);
		i++;
	}
	free(board);
}

char	**ft_expand_board(char **old_board)
{
	int		i;
	int		j;
	int		new_size;
	char	**new_board;

	i = 0;
	j = 0;
	new_size = ft_strlen(old_board[0]) + 1;
	new_board = (char **)malloc(sizeof(char *) * (new_size + 1));
	while (i < new_size)
	{
		new_board[i] = (char *)malloc(sizeof(char) * (new_size + 1));
		while (j < new_size)
		{
			new_board[i][j] = '.';
			j++;
		}
		new_board[i][j] = '\0';
		j = 0;
		i++;
	}
	new_board[i] = NULL;
	ft_erase_board(old_board);
	return (new_board);
}

char	**ft_create_board(int size)
{
	int		j;
	int		k;
	char	**board;

	j = 0;
	k = 0;
	board = (char **)malloc((size + 1) * sizeof(char *));
	while (j < size)
	{
		board[j] = (char *)malloc((size + 1) * sizeof(char));
		while (k < size)
		{
			board[j][k] = '.';
			k++;
		}
		board[j][k] = '\0';
		k = 0;
		j++;
	}
	board[j] = NULL;
	return (board);
}

void	ft_print_board(char **board)
{
	int i;

	i = 0;
	while (board[i])
	{
		ft_putstr(board[i]);
		ft_putchar('\n');
		i++;
	}
}
