/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_moves.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rgendry <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 18:48:51 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/09 18:52:48 by rgendry          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_figure	*ft_move_left(t_figure *coord)
{
	int	i;

	i = 0;
	while (i < 4)
	{
		if (coord->x[0] == 0 || coord->x[1] == 0
			|| coord->x[2] == 0 || coord->x[3] == 0)
			i = 4;
		else
		{
			coord->x[0]--;
			coord->x[1]--;
			coord->x[2]--;
			coord->x[3]--;
		}
		i++;
	}
	return (coord);
}

t_figure	*ft_move_up(t_figure *coord)
{
	int i;

	i = 0;
	while (i < 4)
	{
		if (coord->y[0] == 0 || coord->y[1] == 0
			|| coord->y[2] == 0 || coord->y[3] == 0)
			i = 4;
		else
		{
			coord->y[0]--;
			coord->y[1]--;
			coord->y[2]--;
			coord->y[3]--;
		}
		i++;
	}
	return (coord);
}

t_figure	*ft_move_figure(t_figure *coord)
{
	coord = ft_move_left(coord);
	coord = ft_move_up(coord);
	return (coord);
}
