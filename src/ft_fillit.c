/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fillit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ramory-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/14 16:23:39 by ramory-l          #+#    #+#             */
/*   Updated: 2019/03/14 16:24:02 by ramory-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_set(t_figure *coord, t_fillit *bias, int x, int y)
{
	bias->board[coord->y[0] + y][coord->x[0] + x] = bias->letter;
	bias->board[coord->y[1] + y][coord->x[1] + x] = bias->letter;
	bias->board[coord->y[2] + y][coord->x[2] + x] = bias->letter;
	bias->board[coord->y[3] + y][coord->x[3] + x] = bias->letter;
}

void	ft_reset(char **board, t_figure *coord, int x, int y)
{
	board[coord->y[0] + y][coord->x[0] + x] = '.';
	board[coord->y[1] + y][coord->x[1] + x] = '.';
	board[coord->y[2] + y][coord->x[2] + x] = '.';
	board[coord->y[3] + y][coord->x[3] + x] = '.';
}

int		ft_fillit(t_fillit *bias, int j, int x, int y)
{
	int	result;

	result = 0;
	if (ft_find_dots(bias, j, x, y))
	{
		ft_set(&bias->coord[j], bias, x, y);
		bias->letter++;
		if (j + 1 != bias->num_of_figures)
		{
			if (!(result = ft_fillit(bias, j + 1, 0, 0)))
			{
				ft_reset(bias->board, &bias->coord[j], x, y);
				bias->letter--;
			}
		}
		else
			return (1);
	}
	if (result)
		return (result);
	if (ft_check_cols(bias, j, x + 1, y))
		result = ft_fillit(bias, j, x + 1, y);
	else if (ft_check_rows(bias, j, y + 1))
		result = ft_fillit(bias, j, 0, y + 1);
	return (result);
}

void	ft_init_bias(t_fillit *bias)
{
	bias->x = 0;
	bias->y = 0;
	bias->j = 0;
	bias->letter = 'A';
	bias->num_of_figures = 0;
}
