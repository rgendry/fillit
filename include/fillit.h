/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ramory-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/25 15:37:51 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/14 16:24:54 by ramory-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include "libft.h"
# include <stdio.h>
# include <fcntl.h>

typedef struct	s_figure
{
	int			x[4];
	int			y[4];
}				t_figure;

typedef struct	s_fillit
{
	int			x;
	int			y;
	int			j;
	char		letter;
	int			num_of_figures;
	char		**board;
	t_figure	*coord;
}				t_fillit;

void			ft_init_bias(t_fillit *bias);
int				ft_fillit(t_fillit *bias, int j, int x, int y);
int				ft_check_rows(t_fillit *bias, int num, int y);
int				ft_check_cols(t_fillit *bias, int num, int x, int y);
int				ft_find_dots(t_fillit *bias, int num, int x, int y);
char			**ft_expand_board(char **old_board);
void			ft_print_error(char *program);
t_figure		*ft_open_file(char *argv, int *i);
int				ft_validation(char *buff, t_figure *coord);
char			**ft_create_board(int size);
t_figure		*ft_move_figure(t_figure *coord);
void			ft_print_board(char **board);
void			ft_erase_board(char **board);

#endif
