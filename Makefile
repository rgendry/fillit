# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rgendry <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/01/26 16:27:43 by rgendry           #+#    #+#              #
#    Updated: 2019/03/11 17:39:40 by rgendry          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fillit

SRC_SEARCH = src/*.c main.c

SRC = $(wildcard $(SRC_SEARCH))

OBJ = $(SRC:.c=.o)

HEADERS = -I include -I libft

LIBFLAGS = -L libft -lft

IMFLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME): $(OBJ) libft
	gcc $(OBJ) -o $@ $(LIBFLAGS)

%.o: %.c
	gcc -c $< -o $@ $(IMFLAGS) $(HEADERS) -g

.PHONY: libft
libft:
	make -C libft

.PHONY: clean
clean:
	make clean -C libft
	rm -Rf $(OBJ)

.PHONY: fclean
fclean: clean
	make fclean -C libft
	rm -Rf $(NAME)

.PHONY: re
re: fclean all
