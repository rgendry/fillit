/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ramory-l <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/26 16:18:20 by rgendry           #+#    #+#             */
/*   Updated: 2019/03/14 16:44:41 by ramory-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int argc, char **argv)
{
	t_fillit	bias;

	ft_init_bias(&bias);
	if (argc != 2)
	{
		ft_print_error(argv[0]);
		return (0);
	}
	bias.coord = ft_open_file(argv[1], &bias.num_of_figures);
	if (!bias.coord)
		return (0);
	bias.board = ft_create_board(ft_sqrt(bias.num_of_figures * 4));
	while (!ft_fillit(&bias, bias.j, bias.x, bias.y))
		bias.board = ft_expand_board(bias.board);
	ft_print_board(bias.board);
	ft_erase_board(bias.board);
	free(bias.coord);
	return (0);
}
